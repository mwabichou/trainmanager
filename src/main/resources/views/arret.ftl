<#import "utils.ftl" as u>

<@u.page>
    <div class="table-container">
    <h1>Liste des arrêts</h1>
    <table>
        <tr>
            <th>Numéro de ligne</th>
            <th>Rang</th>
            <th>Ville</th>
            <th>Chrono</th>
            <th>Nombre de pistes réservées</th>
            <th>Longitude</th>
            <th>Latitude</th>
            <th></th>
        </tr>
        <#list arrets as arret>
            <tr>
                <td>${arret.noLigne}</td>
                <td>${arret.rang}</td>
                <td>${arret.ville!}</td>
                <td>${arret.chrono}</td>
                <td>${arret.reserveDesPistes}</td>
                <td>${arret.longitude}</td>
                <td>${arret.latitude}</td>
                <td>
                    <form action="/arret/supprimer?noLigne=${arret.noLigne}&rang=${arret.rang}" method="POST">
                        <input type="submit" value="Supprimer"/>
                    </form>
                </td>
                <td>
                <a href="/arret/ajout?noLigne=${arret.noLigne}&rang=${arret.rang}" class="button ajouter">Ajouter un arrêt avant ce point</a>
                </td>
            </tr>
        </#list>
    </table>

    <p>
        <a href="/arret/ajout" class="button ajouter">Ajouter un arrêt</a>
    </p>
    </div>
</@u.page>
