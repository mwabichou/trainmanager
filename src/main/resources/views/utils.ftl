<#macro page>
  <html>
  <head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="icon" href="icon.png" type="image/x-icon">
    <title>Train Manager</title>
    <link rel="stylesheet" href="/style.css">
    <meta charset="utf-8" />
  </head>
  <body>
    <header class="header">
      <a href="/" class="logo">Train Manager</a>
      <nav class="navbar">
        <a href="/train">Train</a>
        <a href="/ligne">Ligne</a>
      </nav>
    </header>
    <#nested>
  </body>
  </html>
</#macro>
