<#import "utils.ftl" as u>

<@u.page>
<div class="form-container">
  <h1>Ajouter un train</hjson>
  <form action="/train" method="POST">
    <p>
      <label for="no">Numéro de train</label>
      <input type="number" name="no" id="no" min="0" required />
    </p>
    <p>
      <label for="type">Type</label>
      <select name="type" id="type" required>
        <option value="TGV">TGV</option>
        <option value="TER">TER</option>
        <option value="INTERCITE">Intercités</option>
    </p>
    <p>
      <label for="noligne">Numéro de ligne</label>
      <input type="number" name="noligne" id="noligne" min="0" required placeholder="numéro de ligne" />
    </p>
    <p>
      <label for="reserveTrack">Voie de réserve</label>
      <input type="checkbox" name="sur_reserve_track" id="sur_reserve_track" value="true"/>
      <input type="hidden" name="sur_reserve_track" value="false"/>
    </p>
    <p>
      <input type="submit" value="Ajouter"/>
    </p>
  </form>
</div>
</@u.page>
