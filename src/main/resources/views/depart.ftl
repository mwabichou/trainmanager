<#import "utils.ftl" as u>

<@u.page>
<div class="table-container">
    <h1>Départs du train ${trainNumber}</h1>
    <table>
        <tr>
            <th>Numéro Train</th>
            <th>Heure de départ</th>
            <th></th>
        </tr>
        <#list departures as departure>
            <tr>
                <td>${departure.noTrain}</td>
                <td>${departure.heure}</td>
                <td>
                    <form action="/depart/supprimer" method="POST">
                        <input type="hidden" name="notrain" value="${departure.noTrain}" />
                        <input type="hidden" name="heure" value="${departure.heure}" />
                        <input type="hidden" name="noligne" value="${departure.noLigne}" />
                        <input type="submit" value="Supprimer"/>
                    </form>
                </td>
            </tr>
        </#list>
    </table>

    <p>
        <a href="/depart/ajout?notrain=${trainNumber}" class="button">Ajouter</a>
    </p>
</div>
</@u.page>
