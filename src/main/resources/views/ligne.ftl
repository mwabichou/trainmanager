<#import "utils.ftl" as u>

<@u.page>
    <div class="table-container">
    <h1>Liste des lignes</h1>
    <table>
        <tr>
            <th>Numéro</th>
            <th>Nom</th>
            <th></th>
        </tr>
        <#list lines as line>
            <tr>
                <td>${line.noLigne}</td>
                <td>${line.nom!}</td>
                <td>
                    <form action="/ligne/supprimer?number=${line.noLigne}" method="POST">
                        <input type="submit" value="Supprimer"/>
                    </form>
                </td>
                <td>
                    <a href="/arret?noligne=${line.noLigne}" class="button">Voir les arrêts</a>
                </td>
            </tr>
        </#list>
    </table>

    <p>
        <a href="/ligne/ajout" class="button ajouter" >Ajouter</a>
    </p>
    </div>
</@u.page>
