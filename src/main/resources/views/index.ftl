<#import "utils.ftl" as u>
<@u.page>
<div class="map-container">
  <div id="map"></div>
</div>

<script>
var arrets = [
<#if arrets?has_content>
    <#list arrets as arret>
      {
        ville: "${arret.ville}",
        latitude: "${arret.latitude?replace(",", ".")}",
        longitude: "${arret.longitude?replace(",", ".")}"
      }<#if arret_has_next>,</#if>
    </#list>
</#if>
];
</script>


<script>
function initMap() {
  var map = new google.maps.Map(document.getElementById('map'), {
    center: {lat: 45.7772, lng: 3.0870},
    zoom: 6
  });

  if (typeof arrets !== 'undefined' && arrets !== null) {
    for (var i = 0; i < arrets.length; i++) {
      var arret = arrets[i];
      var marker = new google.maps.Marker({
        position: {lat: parseFloat(arret.latitude), lng: parseFloat(arret.longitude)},
        map: map
      });
    }
  }
}

window.onload = initMap;
</script>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCEb4K1nC2O5SCHDwb5mALNLcHYs9bQ8-Y" async defer></script>
</@u.page>