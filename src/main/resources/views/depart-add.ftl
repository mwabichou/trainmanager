<#import "utils.ftl" as u>

<@u.page>
<div class="form-container">
    <h1>Ajouter un départ</h1>
    <form action="/depart/ajout" method="POST">
        <p>
            <label for="notrain">Train Number</label>
            <input type="number" name="notrain" id="notrain" value="${trainNumber}" readonly />
        </p>
        <p>
            <label for="heure">Departure Time</label>
            <input type="text" name="heure" id="heure" min="0" />
        </p>
        <p>
            <label for="noligne">Line Number</label>
            <input type="number" name="noligne" id="noligne" min="0" />
        </p>
        <p>
            <input type="submit" value="Ajouter"/>
        </p>
    </form>
</div>
</@u.page>
